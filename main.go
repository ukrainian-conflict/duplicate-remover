package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/diamondburned/arikawa/v3/api"
	"github.com/diamondburned/arikawa/v3/discord"
	"github.com/diamondburned/arikawa/v3/gateway"
	"github.com/diamondburned/arikawa/v3/session"
	gourl "github.com/goware/urlx"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	flags "github.com/jessevdk/go-flags"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"mvdan.cc/xurls/v2"
	"strings"
	"time"
)

type OptionsStruct struct {
	Token           string   `long:"token" env:"TOKEN" required:"true"`
	Functions       []string `long:"functions" env:"FUNCTIONS" default:"bot" default:"deleter" default:"cleanup"`
	DatabaseURL     string   `long:"database-url" env:"DATABASE_URL" required:"true"`
	WatchChannels   []uint64 `long:"watch-channel" env:"WATCH_CHANNEL" required:"true"`
	HoursToCheck    uint     `long:"hours-to-check" env:"HOURS_TO_CHECK" default:"24"`
	NotificationTTL uint     `long:"notification-ttl" env:"NOTIFICATION_TTL" default:"5"`
}

var options = OptionsStruct{}
var pool *pgxpool.Pool
var rxStrict = xurls.Strict()

func twitter_preprocessor(url string) string {
	url = strings.Split(url, "?")[0]
	parsed, err := gourl.Parse(url)
	if err != nil {
		log.Fatal().Caller().Err(err).Msg("Fatal error")
	}
	parsed.Host = "twitter.com"
	log.Info().Msg(parsed.String())
	return parsed.String()
}

func youtube_preprocessor(url string) string {
	parsed, err := gourl.Parse(url)
	if err != nil {
		log.Fatal().Caller().Err(err).Msg("Fatal error")
	}
	var id string
	switch parsed.Host {
	case "youtu.be":
		id = parsed.Path[1:]
	case "youtube.com":
	case "www.youtube.com":
		id = parsed.Query().Get("v")
	default:
		log.Fatal().Msg("Invalid youtube url")
	}
	return fmt.Sprintf("https://www.youtube.com/watch?v=%s", id)
}

// The url preprocessors
var preprocessors = map[string]func(string) string{
	"twitter.com":      twitter_preprocessor,
	"x.com":            twitter_preprocessor,
	"fxtwitter.com":    twitter_preprocessor,
	"vxtwitter.com":    twitter_preprocessor,
	"fixupx.com":       twitter_preprocessor,
	"fixuptwitter.com": twitter_preprocessor,
	"youtu.be":         youtube_preprocessor,
	"youtube.com":      youtube_preprocessor,
	"www.youtube.com":  youtube_preprocessor,
}

var functions = map[string]func(){
	// Watches for new messages in channel and checks if they are duplicates
	"bot": func() {
		s := session.New(options.Token)
		channels := map[discord.ChannelID]struct{}{}
		for _, i := range options.WatchChannels {
			channels[discord.ChannelID(i)] = struct{}{}
		}
		s.AddHandler(func(c *gateway.MessageCreateEvent) {
			log.Debug().Caller().Interface("msg", c).Msg("Recieved message")
			if _, ok := channels[c.ChannelID]; !ok {
				return
			}

			urls := rxStrict.FindAllString(c.Content, -1)
			if len(urls) == 0 {
				return
			}
			log.Debug().Caller().Interface("urls", urls).Msg("Found URLs")

			conn, err := pool.Acquire(context.Background())
			if err != nil {
				log.Fatal().Caller().Err(err).Msg("Fatal error")
			}

			// Check if the URL already exists
			checkAfter := discord.NewSnowflake(time.Now().Add(time.Duration(-options.HoursToCheck) * time.Hour))
			for _, url := range urls {
				// First preprocess the url
				if url, err = gourl.NormalizeString(url); err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				parsed, err := gourl.Parse(url)
				if err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				if funct, ok := preprocessors[parsed.Hostname()]; ok {
					url = funct(url)
				}

				var messageID discord.MessageID
				log.Debug().Caller().Interface("url", url).Msg("Doing URL")
				err = conn.QueryRow(context.Background(), "select id from message where link=$1 and channel = $2 and id > $3;", url, c.ChannelID, checkAfter).Scan(&messageID)
				if err != nil && err != pgx.ErrNoRows {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				// We found a repost
				if err != pgx.ErrNoRows {
					// Post the ping message
					log.Debug().Interface("Event", c).Msg("Doing event")
					msg, err := s.SendMessage(c.ChannelID, c.Author.Mention(), discord.Embed{
						Title:       "Repost Detected",
						Description: fmt.Sprintf("We have detected that your message contains a repost. The original message is here: https://discord.com/channels/%s/%s/%s", c.GuildID, c.ChannelID, messageID),
					})
					if err != nil {
						log.Fatal().Caller().Err(err).Msg("Fatal error")
					}
					// Insert into the table
					_, err = conn.Exec(context.Background(), "insert into ephemeral (id, channel) values ($1, $2);", msg.ID, msg.ChannelID)
					if err != nil {
						log.Fatal().Caller().Err(err).Msg("Fatal error")
					}
					// Delete the offending message
					err = s.DeleteMessage(c.ChannelID, c.ID, "Duplicate Removed")
					if err != nil {
						log.Fatal().Caller().Err(err).Msg("Fatal error")
					}
					return
				}
			}

			// Insert it into the database
			tx, err := conn.Begin(context.Background())
			if err != nil {
				log.Fatal().Caller().Err(err).Msg("Fatal error")
			}
			for _, url := range urls {
				// First preprocess the url
				if url, err = gourl.NormalizeString(url); err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				parsed, err := gourl.Parse(url)
				if err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				if funct, ok := preprocessors[parsed.Hostname()]; ok {
					url = funct(url)
				}

				// Then insert it into the database
				if _, err := tx.Exec(context.Background(), "insert into message (id, channel, link) values ($1, $2, $3);", c.ID, c.ChannelID, url); err != nil {
					tx.Rollback(context.Background())
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
			}
			if err = tx.Commit(context.Background()); err != nil {
				tx.Rollback(context.Background())
				log.Fatal().Caller().Err(err).Msg("Fatal error")
			}
			conn.Release()
		})

		// Add the needed Gateway intents.
		s.AddIntents(gateway.IntentGuildMessages)

		if err := s.Open(context.Background()); err != nil {
			log.Fatal().Err(errors.Wrap(err, "err")).Msg("Failed to connect")
		}
		defer s.Close()

		u, err := s.Me()
		if err != nil {
			log.Fatal().Err(errors.Wrap(err, "err")).Msg("Failed to get myself")
		}

		log.Info().Msgf("Started as %s", u.Username)

		// Block forever.
		select {}
	},
	// Delete notification messages
	"deleter": func() {
		c := api.NewClient(options.Token)
		tmpcon, err := pool.Acquire(context.Background())
		if err != nil {
			log.Fatal().Caller().Err(err).Msg("Fatal error")
		}

		// On startup, go ahead and find all ephemeral messages and start them
		var count int
		if err = tmpcon.QueryRow(context.Background(), "select count(*) from ephemeral;").Scan(&count); err != nil {
			log.Fatal().Caller().Err(err).Msg("Fatal error")
		}
		log.Debug().Interface("count", count).Msg("Found messages on startup")
		for ; count > 0; count-- {
			go func() {
				poolconn, err := pool.Acquire(context.Background())
				defer poolconn.Release()
				log.Debug().Msg("Acquired on startup")
				if err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				tx, err := poolconn.Begin(context.Background())
				if err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				var id, channel uint64
				if err := tx.QueryRow(context.Background(), "select id, channel from ephemeral limit 1 for update skip locked;").Scan(&id, &channel); err != nil {
					// TODO: check if we didn't acquire it and ignore
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				// Sleep until its time to delete
				log.Debug().Interface("sleep", discord.MessageID(id).Time().Add(time.Duration(options.NotificationTTL)*time.Second).Sub(time.Now()).Seconds()).Msg("Sleeping for")
				time.Sleep(discord.MessageID(id).Time().Add(time.Duration(options.NotificationTTL) * time.Second).Sub(time.Now()))
				// Delete it
				if err = c.DeleteMessage(discord.ChannelID(channel), discord.MessageID(id), "ephemeral"); err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}

				// Remove from the database
				if _, err := tx.Exec(context.Background(), "delete from ephemeral where id = $1;", id); err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				if err := tx.Commit(context.Background()); err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
			}()
		}

		conn := tmpcon.Hijack()
		if _, err := conn.Exec(context.Background(), "listen ephemeral"); err != nil {
			log.Fatal().Caller().Err(err).Msg("Fatal error")
		}

		// Listen for stuff
		for {
			notification, err := conn.WaitForNotification(context.Background())
			if err != nil {
				log.Fatal().Caller().Err(err).Msg("Fatal error")
			}

			var payload struct {
				Action string `json:"action"`
				Record struct {
					ID      uint64 `json:"id"`
					Channel uint64 `json:"channel"`
				} `json:"record"`
			}
			if err := json.Unmarshal([]byte(notification.Payload), &payload); err != nil {
				log.Fatal().Caller().Err(err).Msg("Fatal error")
			}
			log.Info().Interface("notification", notification).Interface("payload", payload).Msg("Got notification")
			if payload.Action != "insert" {
				continue
			}

			go func() {
				poolconn, err := pool.Acquire(context.Background())
				if err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				tx, err := poolconn.Begin(context.Background())
				if err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				if _, err := tx.Exec(context.Background(), "select id from ephemeral where id=$1 for update;", payload.Record.ID); err != nil {
					// TODO: check if we didn't acquire it and ignore
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				// Sleep until its time to delete
				log.Debug().Interface("sleep", discord.MessageID(payload.Record.ID).Time().Add(time.Duration(options.NotificationTTL)*time.Second).Sub(time.Now()).Seconds()).Msg("Sleeping for")
				log.Debug().Interface("deletetime", discord.MessageID(payload.Record.ID).Time().Add(time.Duration(options.NotificationTTL)*time.Second).String()).Msg("Delete Time")
				time.Sleep(discord.MessageID(payload.Record.ID).Time().Add(time.Duration(options.NotificationTTL) * time.Second).Sub(time.Now()))
				// Delete it
				if err = c.DeleteMessage(discord.ChannelID(payload.Record.Channel), discord.MessageID(payload.Record.ID), "ephemeral"); err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}

				// Remove from the database
				if _, err := tx.Exec(context.Background(), "delete from ephemeral where id = $1;", payload.Record.ID); err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
				if err := tx.Commit(context.Background()); err != nil {
					log.Fatal().Caller().Err(err).Msg("Fatal error")
				}
			}()
		}
	},
	// Removes old links from the database
	"cleanup": func() {
		conn, err := pool.Acquire(context.Background())
		if err != nil {
			log.Fatal().Caller().Err(err).Msg("Fatal error")
		}
		checkAfter := discord.NewSnowflake(time.Now().Add(time.Duration(-options.HoursToCheck) * time.Hour))
		if _, err := conn.Query(context.Background(), "delete from message where id < $1;", checkAfter); err != nil {
			log.Fatal().Caller().Err(err).Msg("Fatal error")
		}
		conn.Release()
		time.Sleep(1 * time.Hour)
	},
}

func main() {
	zerolog.SetGlobalLevel(zerolog.DebugLevel)
	var err error
	// Get config from environment
	parser := flags.NewParser(&options, flags.Default)
	if _, err = parser.Parse(); err != nil {
		if flagsErr, ok := err.(*flags.Error); ok && flagsErr.Type == flags.ErrHelp {
			return
		} else {
			log.Fatal().Caller().Err(err).Msg("Fatal error")
		}
	}

	// Setup pgxpool and test the database connection
	pool, err = pgxpool.New(context.Background(), options.DatabaseURL)
	if err != nil {
		log.Fatal().Caller().Err(err).Msg("Fatal error")
	}
	conn, err := pool.Acquire(context.Background())
	if err != nil {
		log.Fatal().Caller().Err(err).Msg("Fatal error")
	}
	err = conn.Ping(context.Background())
	if err != nil {
		log.Fatal().Caller().Err(err).Msg("Fatal error")
	}
	conn.Release()

	// Start all the background stuff
	for _, function := range options.Functions {
		funct, ok := functions[function]
		if !ok {
			log.Fatal().Msgf("No such function: %s", function)
		}
		go funct()
	}
	select {}
}
