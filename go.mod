module gitlab.com/ukrainian-conflict/duplicate-remover

go 1.21.0

require (
	github.com/diamondburned/arikawa/v3 v3.3.3
	github.com/goware/urlx v0.3.2
	github.com/jackc/pgx/v5 v5.4.3
	github.com/jessevdk/go-flags v1.5.0
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.31.0
	mvdan.cc/xurls/v2 v2.5.0
)

require (
	github.com/PuerkitoBio/purell v1.1.1 // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/gorilla/schema v1.2.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.1 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/crypto v0.14.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sync v0.4.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	golang.org/x/time v0.3.0 // indirect
)
